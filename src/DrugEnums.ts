export enum DrugFrequencyType {
  EveryXYs = "EveryXYs",
  XTimesAY = "XTimesAY",
  TimeOfDay = "TimeOfDay",
  CustomFrequency = "CustomFrequency"
}

export enum DrugDurationType {
  None = "None",
  Hour = "Hour",
  Day = "Day",
  Week = "Week",
  Month = "Month",
  Year = "Year"
}

export enum TimeUnit {
  Hour = "Hour",
  Day = "Day",
  Week = "Week",
  Month = "Month",
  Year = "Year"
}

export enum DrugRoute {
  Injection = "Injection",
  Nasal = "Nasal",
  Optical = "Optical",
  Oral = "Oral",
  Rectal = "Rectal",
  Topical = "Topical"
}

export enum DrugMeasurementUnit {
  None = "None",
  ml = "ml",
  tsp = "tsp",
  tbsp = "tbsp",
  FTU = "FTU",
  tablet = "tablet",
  capsule = "capsule",
  injection = "injection",
  drop = "drop",
  puff = "puff",
  unit = "unit"
}

export enum DrugForm {
  Application = "Application",
  Capsule = "Capsule",
  Drop = "Drop",
  Eye_Ointment = "Eye Ointment",
  Inhaler = "Inhaler",
  Injectible = "Injectible",
  Lotion = "Lotion",
  Nasal_Spray = "Nasal Spray",
  Ointment = "Ointment",
  Oral_Suspension = "Oral Suspension",
  Patch = "Patch",
  Powder = "Powder",
  Soap = "Soap",
  Solution = "Solution",
  Suppository = "Suppository",
  Suspension = "Suspension",
  Syrup = "Syrup",
  Tablet = "Tablet",
  Vaccine = "Vaccine"
}
