import {
  DrugForm,
  DrugRoute,
  TimeUnit,
  DrugFrequencyType,
  DrugDurationType,
  DrugMeasurementUnit
} from './DrugEnums';
import {
  Model,
  ModelObject,
  OptionModel
} from 'models';

export * from './DrugEnums';

// class Validation {
//   public static isEmpty(val: any) {
//     return (
//       val === null ||
//       val === undefined ||
//       (typeof val === 'string' && val.trim() === '') ||
//       (typeof val === 'object' && Validation.isEmptyObject(val))
//     );
//   }

//   public static isBoolean(val: any) {
//     return typeof val === 'boolean'
//   }

//   public static isNumber(val: any) {
//     return typeof val === 'number'
//   }

//   private static isEmptyObject(obj: object) {
//     for (var key in obj) {
//       if (obj.hasOwnProperty(key))
//         return false;
//     }
//     return true;
//   }
// }

export class Drug extends Model {
  public id: string;
  public genericName: string;
  public brandName: string;
  public measurementUnit: DrugMeasurementUnit;
  public form: DrugForm;
  public route: DrugRoute;

  public migrate(saved: ModelObject) {
    return saved;
  }
}
Model.create(Drug, {
  version: 1,
  properties: [{
    name: "id",
    type: "string",
    optional: false
  },
  {
    name: "genericName",
    type: "string",
    optional: false
  },
  {
    name: "brandName",
    type: "string"
  },
  {
    name: "measurementUnit",
    type: "string",
    optional: false,
    whitelist: [
      DrugMeasurementUnit.FTU, DrugMeasurementUnit.ml, DrugMeasurementUnit.None,
      DrugMeasurementUnit.tbsp, DrugMeasurementUnit.tsp, DrugMeasurementUnit.capsule,
      DrugMeasurementUnit.drop, DrugMeasurementUnit.injection, DrugMeasurementUnit.puff,
      DrugMeasurementUnit.tablet, DrugMeasurementUnit.unit
    ]
  },
  {
    name: "form",
    type: "string",
    optional: false,
    whitelist: [DrugForm.Application, DrugForm.Capsule, DrugForm.Drop, DrugForm.Eye_Ointment,
    DrugForm.Inhaler, DrugForm.Injectible, DrugForm.Lotion, DrugForm.Nasal_Spray,
    DrugForm.Ointment, DrugForm.Oral_Suspension, DrugForm.Patch, DrugForm.Powder,
    DrugForm.Soap, DrugForm.Solution, DrugForm.Suppository, DrugForm.Suspension,
    DrugForm.Syrup, DrugForm.Tablet, DrugForm.Vaccine
    ]
  }
  ]
});

export class DrugDosage extends Model {
  public type: DrugMeasurementUnit;
  public value: number;

  public migrate(saved: ModelObject) {
    return saved;
  }

  public toString = (): string => {
    return this.value + " " + this.type + (this.value > 1 ? "s" : "");
  }
}
Model.create(DrugDosage, {
  version: 1,
  properties: [{
    name: "type",
    type: "string",
    optional: false,
    whitelist: [
      DrugMeasurementUnit.FTU, DrugMeasurementUnit.ml, DrugMeasurementUnit.None,
      DrugMeasurementUnit.tbsp, DrugMeasurementUnit.tsp, DrugMeasurementUnit.capsule,
      DrugMeasurementUnit.drop, DrugMeasurementUnit.injection, DrugMeasurementUnit.puff,
      DrugMeasurementUnit.tablet, DrugMeasurementUnit.unit
    ]
  },
  {
    name: "value",
    type: "number",
    optional: false
  }
  ]
});

export class TimeOfDay extends Model {
  isMorning: boolean;
  isNoon: boolean;
  isEvening: boolean;
  isNight: boolean;

  public migrate(saved: ModelObject) {
    return saved;
  }

  public toString = (): string => {
    let str = [];
    if (this.isMorning) str.push("morning");
    if (this.isEvening) str.push("evening");
    if (this.isNight) str.push("night");
    if (this.isNoon) str.push("noon");
    let res = str.map((value, index, array) => {
      if (array.length > 3)
        return value.substr(0, 4) + ((value.length > 4) ? "." : "")
      else
        return value;
    }).reduce((previousValue, currentValue, currentIndex, array) => {
      if (currentIndex == (array.length - 1) && array.length > 1) {
        return previousValue + " and " + currentValue;
      } else if (currentIndex == 0) {
        return currentValue;
      } else {
        return previousValue + ", " + currentValue;
      }
    }, "");
    return res === undefined || res === null ? "n/a" : res;
  }
}
Model.create(TimeOfDay, {
  version: 1,
  properties: [{
    name: "isMorning",
    type: "boolean",
    default: false
  },
  {
    name: "isNoon",
    type: "boolean",
    default: false
  },
  {
    name: "isEvening",
    type: "boolean",
    default: false
  },
  {
    name: "isNight",
    type: "boolean",
    default: false
  },
  ]
});

export class XTimesAY extends Model {
  public type: TimeUnit; //Y. e.g. Day
  public value: number; //X. e.g. 4
  //Together, 4 times a Day.

  public migrate(saved: ModelObject) {
    return saved;
  }
  public toString = (): string => {
    return this.value + " times a " + this.type;
  }
}
Model.create(XTimesAY, {
  version: 1,
  properties: [{
    name: "type",
    type: "string",
    default: TimeUnit.Day,
    whitelist: [
      TimeUnit.Day, TimeUnit.Hour, TimeUnit.Month, TimeUnit.Week, TimeUnit.Year
    ]
  },
  {
    name: "value",
    type: "number",
    optional: false
  }
  ]
});

export class EveryXYs extends Model {
  public type: TimeUnit; //Y. e.g. Hour
  public value: number; //X. e.g. 2
  //Together, Every 2 Hours

  public migrate(saved: ModelObject) {
    return saved;
  }

  public toString = (): string => {
    return "Every " + this.value + " " + this.type + (this.value > 1 ? "s" : "");
  }
}
Model.create(EveryXYs, {
  version: 1,
  properties: [{
    name: "type",
    type: "string",
    optional: false,
    whitelist: [
      TimeUnit.Day, TimeUnit.Hour, TimeUnit.Month, TimeUnit.Week, TimeUnit.Year
    ]
  },
  {
    name: "value",
    type: "number",
    optional: false
  }
  ]
})

export class CustomFrequency extends Model {
  public text: string;

  public migrate(saved: ModelObject) {
    return saved;
  }

  public toString = (): string => {
    return this.text;
  }
}
Model.create(CustomFrequency, {
  version: 1,
  properties: [{
    name: "text",
    type: "string",
    optional: false
  }]
})

export class DrugFrequency extends OptionModel {
  toString = (): string => {
    return this.getValue().toString();
  }
}

Model.create(DrugFrequency, {
  version: 1,
  optionModels: [
    { type: XTimesAY, name: DrugFrequencyType.XTimesAY },
    { type: EveryXYs, name: DrugFrequencyType.EveryXYs },
    { type: CustomFrequency, name: DrugFrequencyType.CustomFrequency },
  ]
});

export class DrugRelationToMeals extends Model {
  public beforeMeals: boolean;
  public withMeals: boolean;
  public afterMeals: boolean;

  public migrate(saved: ModelObject) {
    return saved;
  }

  public toString = (): string => {
    if (this.beforeMeals)
      return "before";
    if (this.afterMeals)
      return "after";
    if (this.withMeals)
      return "with";

    return "your convenience";
  }
}
Model.create(DrugRelationToMeals, {
  version: 1,
  properties: [{
    name: "beforeMeals",
    type: "boolean",
    default: false
  },
  {
    name: "withMeals",
    type: "boolean",
    default: false
  },
  {
    name: "afterMeals",
    type: "boolean",
    default: false
  }
  ]
})

export class DrugDuration extends Model {
  public type: DrugDurationType;
  public value: number;

  public migrate(saved: ModelObject) {
    return saved;
  }
  public toString = (): string => {
    return this.value + " " + this.type + ((this.value > 1) ? "s" : "");
  }
}
Model.create(DrugDuration, {
  version: 1,
  properties: [{
    name: "type",
    type: "string",
    optional: false,
    whitelist: [
      DrugDurationType.Day, DrugDurationType.Hour, DrugDurationType.Month, DrugDurationType.None,
      DrugDurationType.Week, DrugDurationType.Year
    ]
  },
  {
    name: "value",
    type: "number",
    optional: false
  }
  ]
});

export class DrugIntake extends Model {
  public dosage: DrugDosage;
  public frequency: DrugFrequency;
  public duration: DrugDuration;
  public timeOfDay: TimeOfDay;
  public relationToMeals: DrugRelationToMeals;

  public migrate(saved: ModelObject) {
    return saved;
  }

  public toString = (): string => {
    return this.dosage.toString() + " " + this.relationToMeals + " " + this.frequency.toString() + " for " + this.duration.toString();
  }
}
Model.create(DrugIntake, {
  version: 1,
  properties: [{
    name: "dosage",
    type: DrugDosage,
    optional: false
  },
  {
    name: "frequency",
    type: DrugFrequency,
    optional: false
  },
  {
    name: "duration",
    type: DrugDuration,
    optional: false
  },
  {
    name: "timeOfDay",
    type: TimeOfDay,
    optional: true
  },
  {
    name: "relationToMeals",
    type: DrugRelationToMeals,
    optional: false
  }
  ]
})

export class PrescriptionItem extends Model {
  public drug: Drug;
  public intakes: DrugIntake[];
  public SOS: boolean;
  public comment: string;

  public migrate(saved: ModelObject) {
    return saved;
  }
}

Model.create(PrescriptionItem, {
  version: 1,
  properties: [{
    name: "drug",
    type: Drug,
    optional: false
  },
  {
    name: "intakes",
    type: DrugIntake,
    optional: false,
    isArray: true
  },
  {
    name: "SOS",
    type: "boolean",
    default: false
  },
  {
    name: "comment",
    type: "string"
  }
  ]
})

export class Age extends Model {
  public type: TimeUnit;
  public value: number;

  public migrate(saved: ModelObject) {
    return saved;
  }
  public toString = (): string => {
    return this.value + " " + this.type + ((this.value > 1) ? "s" : "");
  }
}
Model.create(Age, {
  version: 1,
  properties: [{
    name: "type",
    type: "string",
    optional: false,
    whitelist: [
      TimeUnit.Day, TimeUnit.Hour, TimeUnit.Month, TimeUnit.Week, TimeUnit.Year
    ]
  },
  {
    name: "value",
    type: "number",
    optional: false
  }
  ]
});

export class PrescriptionDoctor extends Model {
  public doctorId: string;
  public fullName: string;
  public speciality: string;
  public country: string;
  public registrationNo: string;

  public migrate(saved: ModelObject) {
    return saved;
  }
}
Model.create(PrescriptionDoctor, {
  version: 1,
  properties: [{
    name: "doctorId",
    type: "string",
    optional: false
  }, {
    name: "fullName",
    type: "string",
    optional: false
  }, {
    name: "speciality",
    type: "string",
    optional: false
  },
  {
    name: "country",
    type: "string",
    optional: false
  }, {
    name: "registrationNo",
    type: "string",
    optional: false
  }]
});

export class PrescriptionPatient extends Model {
  public patientId: string;
  public fullName: string;
  public gender: string;
  public age: Age;

  public migrate(saved: ModelObject) {
    return saved;
  }
}
Model.create(PrescriptionPatient, {
  version: 1,
  properties: [{
    name: "patientId",
    type: "string",
    optional: false
  }, {
    name: "fullName",
    type: "string",
    optional: false
  }, {
    name: "gender",
    type: "string",
    optional: false
  }, {
    name: "age",
    type: Age,
    optional: false
  }]
});

export class Prescription extends Model {

  //Consultation Info
  public consultationId: string;

  //Patient Info
  public patient: PrescriptionPatient;
  public allergies: string[];

  public doctor: PrescriptionDoctor;

  //Prescription Info
  public items: PrescriptionItem[];
  public comment: string;
  public referenceNo: string;
  public issuedDate: Date;
  public expiryDate: Date;
  public investigations: string[];

  public migrate(saved: ModelObject) {
    if (saved._version == 1) {
      saved.patient = {};

      saved.patient.patientId = saved.patientId;
      saved.patient.fullName = saved.patientFullName;
      saved.patient.age = saved.age;
      saved.patient.gender = "Not Specified";

      delete saved.patientId;
      delete saved.patientFullName;
      delete saved.age;

      saved.doctor = {};

      saved.doctor.doctorId = saved.doctorId;
      saved.doctor.fullName = saved.doctorFullName;
      saved.doctor.speciality = "";
      saved.doctor.country = "Sri Lanka";
      saved.doctor.registrationNo = "";

      delete saved.doctorId;
      delete saved.docotorFullName;

      saved.referenceNo = "";
      saved.date = Date.now();
      saved._version = 2;
    }
    if (saved._version == 2) {
      saved.issuedDate = saved.date;
      saved.expiryDate = saved.date + (86400 * 7);
      delete saved.date;
    }
    return saved;
  }
}
Model.create(Prescription, {
  version: 2,
  collection: "Prescriptions",
  properties: [{
    name: "consultationId",
    type: "string",
    optional: false
  },
  {
    name: "patient",
    type: PrescriptionPatient,
    optional: false
  },
  { name: "allergies", type: "string", isArray: true, optional: false },
  {
    name: "doctor",
    type: PrescriptionDoctor,
    optional: false
  },
  {
    name: "items",
    type: PrescriptionItem,
    isArray: true
  },
  {
    name: "comment",
    type: "string"
  },
  {
    name: "referenceNo",
    type: "string",
    optional: false
  }, {
    name: "issuedDate",
    type: "date",
    optional: false
  }, {
    name: "expiryDate",
    type: "date",
    optional: false
  },
  { name: "investigations", type: "string", isArray: true, optional: false }
  ]
});